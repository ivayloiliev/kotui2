###################
KOT UI
###################

Basic word puzzle game developed by Angular and CodeIgniter. 

PREVIEW: http://ivailoiliev.com/kotui2



**************
Installation
**************

Configure the database in folder /application/config/database.php
and use the SQL image in the root folder (kotui.sql).