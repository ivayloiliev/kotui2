<?php
class public_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
    function get_reservations_for_calendar($date,$calendar_id){
      $this->db->order_by('date');
      $this->db->where('date>=',$date." 00:00:00");
      $this->db->where('date<=',$date." 23:59:59");
      $this->db->where('calendar_id',$calendar_id);
      $query = $this->db->get('reservations');
      return $query->result();
    }
    function get_calendar_info($id,$date_start,$date_end){
      $this->db->select("start_work as startWork, stop_work as stopWork, datediff('".$date_end."', '".$date_start."') as days");
      $this->db->where('id',$id);
      $query = $this->db->get('calendar');
      return $query->result();
    }
    function get_services_for_calendar($id){
      $this->db->where('id_calendar',$id);
      $query = $this->db->get('services');
      return $query->result();
    }
    function reserve_service($date,$id,$duration,$comments){
      $user_id = $this->tank_auth->get_user_id();
      $data = array(
         'date' => $date ,
         'calendar_id' => $id,
         'user_id' => $user_id,
         'duration' => $duration,
         'comment' => $comments,
         'status' => 1
      );
      $this->db->insert('reservations', $data);
    }
} ?>
