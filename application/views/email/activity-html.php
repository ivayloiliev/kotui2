<html>
  <body bgcolor="#cacaca">
<div style="width:600px; background-color:#F4F4F4;margin:auto; font-family:Verdana, Geneva, sans-serif; font-size:12px; color:#333;padding:20px; border:1px solid #CCC">
<p align="center"><strong>APPLICATION FOR FINANCIAL SUPPORT of  CILECT activities 2013-2014</strong></p>
<div>
  <strong>Name of the activity: </strong><br/><?=$name;?></div>
<p><strong>Date Submitted: <br/></strong><?=$date;?><br />
  <strong>Category Number(s)  (choose all that apply):</strong> <br />
  <?php if(isset($q1211)) echo "YES "; ?> 1211 Inter-Regional  Participation at Regional Events (Conferences, Symposia, Workshops)<br />
  <?php if(isset($q1212)) echo "YES "; ?> 1212 Inter-Regional Teaching Staff Exchange (for periods of at least 2  weeks) <br />
  <?php if(isset($q1213)) echo "YES "; ?>  1213 Training  the Trainers Inter-Regional Initiatives <br />
  <?php if(isset($q1220)) echo "YES "; ?>  1220 Inter-Regional Students  Collaboration <br />
  <?php if(isset($q1231)) echo "YES "; ?> 1231 Curricula  Development for Inter-Regional Post-Graduate Programs <br />
  <?php if(isset($q1232)) echo "YES "; ?> 1232 Dissemination  of Events Results <br />
  <?php if(isset($q1233)) echo "YES "; ?> 1233 Translations  of Teaching Materials </p>
<p><strong>Full Member School – Major  (Leading) Partner:<br />
 <br/></strong><?=$major;?></p>
<p><strong>Full Member School(s) –  Partner(s):<br />
<br/></strong><?=$partner;?></p>
<p><strong>&nbsp;</strong></p>
<p><strong>Other Partners (if any):<br />
<br/></strong><?=$other;?></p>
<p><strong>Person in Charge of the Activity  (Chair): <br />
<br/></strong><?=$chair;?><br />
  <strong>Description of the Activity  (in not more than 100 words):</strong></p>
<br/><?=$desc;?>
<p><strong>Estimated Schedule: <br />
<br/><?=$schedule;?></strong></p>
<p><strong>Potential Benefits of the Activity  (in not more than 100 words):</strong></p>

  <br/><?=$benefits;?>

<p><strong>&nbsp;</strong></p>
<p><strong>Proposal for Dissemination  of the Results:<br />
<br/></strong><?=$proposal;?></p>
<p><strong>Budget and Financial Plan in  EUR (in separate attachment):</strong><br />
<br/><?=$budget;?><br />
  <strong>(including breakdown of the  financial assistance required from CILECT )</strong></p>
<div style="text-align:center">
  </div>
</p>

</div>
IP OF SENDER:<?=$_SERVER['REMOTE_ADDR'];?><br/>
BROWSER: <?=$_SERVER['HTTP_USER_AGENT'];?>

</div>
  </body>
</html>
