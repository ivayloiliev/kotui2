<html> 
  <body bgcolor="#cacaca"> 
<div style="width:600px; background-color:#F4F4F4;margin:auto; font-family:Verdana, Geneva, sans-serif; font-size:12px; color:#333;padding:20px; border:1px solid #CCC">
<p align="center"><strong>THE EIGHTH CILECT PRIZE COMPETITION</strong><br />
  <strong>2014</strong><br />
  <strong>ENTRY FORM</strong></p>
<p align="right">&nbsp;</p>
<p align="left"><strong><em><u>To be returned to:</u></em></strong></p>
<strong><em><u><br clear="all" />
</u></em></strong>
<p>CILECT  PRIZE 2014<br />
  Stanislav  Semerdjiev, Executive Director, CILECT<br />
  108a  Rakovsky str., NATFA, 1000 Sofia, BULGARIA <br />
  <a href="mailto:CILECT.prize@gmail.com">CILECT.prize@gmail.com</a>; <a href="mailto:stanislav.semerdjiev@gmail.com">stanislav.semerdjiev@gmail.com</a></p>
<p align="center"><u>Deadline:  FEBRUARY 22nd,  2013<strong></strong><br />
 
<table border="0" cellspacing="0" cellpadding="0" width="100%">
  <tr>
    <td><strong>Title of Film in English:</strong></td>
    <td><?=$title_english;?></td>
  </tr>
  <tr>
    <td><strong>Title  of Film in Original Language:</strong></td>
    <td><?=$title_orig;?></td>
  </tr>
  <tr>
    <td colspan="2"><strong>Format:
        <?=$format;?>
Length (min.):
<?=$lenght;?>
Year of Production:
<?=$year;?>
    </strong></td>
    </tr>
  <tr>
    <td><strong>School/Country:</strong></td>
    <td><strong>
      <?=$school;?>
    </strong></td>
  </tr>
  <tr>
    <td><strong>Producer:</strong></td>
    <td><strong>
      <?=$producer;?>
    </strong></td>
  </tr>
  <tr>
    <td><strong>Screenwriter</strong></td>
    <td><strong>
      <?=$screen;?>
    </strong></td>
  </tr>
  <tr>
    <td><strong>Director</strong></td>
    <td><strong>
      <?=$director;?>
    </strong></td>
  </tr>
  <tr>
    <td><strong>Director  of Photography</strong></td>
    <td><strong>
      <?=$photo;?>
    </strong></td>
  </tr>
  <tr>
    <td><strong>Editor</strong></td>
    <td><strong>
      <?=$editor;?>
    </strong></td>
  </tr>
  <tr>
    <td><strong>Sound</strong></td>
    <td><strong>
      <?=$sound;?>
    </strong></td>
  </tr>
  <tr>
    <td><strong>Leading  Actors</strong></td>
    <td><strong>
      <?=$actors;?>
    </strong></td>
  </tr>
</table>
<p>
  <strong>Synopsis  (up to 50 words):</strong><br />

<?=$synopsis;?>
</p>
<p><strong>Section for which the film should be  considered:</strong><strong> </strong><br />
  <?=$section;?>
<p><strong>&nbsp;</strong>
<div style="text-align:center">
</div>
</p>
IP OF SENDER:<?=$_SERVER['REMOTE_ADDR'];?><br/>
BROWSER: <?=$_SERVER['HTTP_USER_AGENT'];?> 

</div>
  </body> 
</html> 