<?php $this->load->view('template/header');  ?>
<?php
$password = array(
	'name'	=> 'password',
	'id'	=> 'password',
	'size'	=> 30,
	'placeholder' => 'Password',
	'title' => 'Password',
);
$email = array(
	'name'	=> 'email',
	'id'	=> 'email',
	'value'	=> set_value('email'),
	'maxlength'	=> 80,
	'size'	=> 30,
	'placeholder' => 'Email',
	'title' => 'Email',
);
?>
<?php $this->load->view('template/header');  ?>
    <div id="left">
<?php echo form_open($this->uri->uri_string()); ?>
<h1>Change Email</h1>
<table>
	<tr>

		<td><?php echo form_password($password); ?></td>
		<td style="color: red;"><?php echo form_error($password['name']); ?><?php echo isset($errors[$password['name']])?$errors[$password['name']]:''; ?></td>
	</tr>
	<tr>

		<td><?php echo form_input($email); ?></td>
		<td style="color: red;"><?php echo form_error($email['name']); ?><?php echo isset($errors[$email['name']])?$errors[$email['name']]:''; ?></td>
	</tr>
</table>
<?php echo form_submit('change', 'Send confirmation email'); ?>
<?php echo form_close(); ?>


<?php 

 $this->load->view('template/rightbar');  ?>
<?php $this->load->view('template/footer');  ?>