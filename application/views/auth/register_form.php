<?php
if ($use_username) {
	$username = array(
		'name'	=> 'username',
		'id'	=> 'username',
		'value' => set_value('username'),
		'maxlength'	=> $this->config->item('username_max_length', 'tank_auth'),
		'size'	=> 30,
		'placeholder' =>'Username'
	);
}
$email = array(
	'name'	=> 'email',
	'id'	=> 'email',
	'value'	=> set_value('email'),
	'maxlength'	=> 80,
	'size'	=> 30,
	'placeholder' =>'Email',
	'title' =>'Email'
);
$password = array(
	'name'	=> 'password',
	'id'	=> 'password',
	'value' => set_value('password'),
	'maxlength'	=> $this->config->item('password_max_length', 'tank_auth'),
	'size'	=> 30,
	'placeholder' =>'Password',
	'title' =>'Password'
);
$confirm_password = array(
	'name'	=> 'confirm_password',
	'id'	=> 'confirm_password',
	'value' => set_value('confirm_password'),
	'maxlength'	=> $this->config->item('password_max_length', 'tank_auth'),
	'size'	=> 30,
	'placeholder' =>'Password again',
	'title' =>'Password again'
);
$captcha = array(
	'name'	=> 'captcha',
	'id'	=> 'captcha',
	'maxlength'	=> 8,
);
?><?php $this->load->view('template/header');  ?>
<?php echo form_open($this->uri->uri_string()); ?>
    <div id="left">
    <div class="fifty">
<?php echo form_open($this->uri->uri_string()); ?>
<h1>Register</h1>
<table>
	<?php if ($use_username) { ?>
	<tr>

		<td><?php echo form_input($username); ?></td>
		<td style="color: red;"><?php echo form_error($username['name']); ?><?php echo isset($errors[$username['name']])?$errors[$username['name']]:''; ?></td>
	</tr>
	<?php } ?>
	<tr>

		<td><?php echo form_input($email); ?></td>
		<td style="color: red;"><?php echo form_error($email['name']); ?><?php echo isset($errors[$email['name']])?$errors[$email['name']]:''; ?></td>
	</tr>
	<tr>

		<td><?php echo form_password($password); ?></td>
		<td style="color: red;"><?php echo form_error($password['name']); ?></td>
	</tr>
	<tr>

		<td><?php echo form_password($confirm_password); ?></td>
		<td style="color: red;"><?php echo form_error($confirm_password['name']); ?></td>
	</tr>

	<?php if ($captcha_registration) {
		if ($use_recaptcha) { ?>
	<tr>
		<td >
			<div id="recaptcha_image"></div>
		</td>
		<td>
			<a href="javascript:Recaptcha.reload()">Get another CAPTCHA</a>
			<div class="recaptcha_only_if_image"><a href="javascript:Recaptcha.switch_type('audio')">Get an audio CAPTCHA</a></div>
			<div class="recaptcha_only_if_audio"><a href="javascript:Recaptcha.switch_type('image')">Get an image CAPTCHA</a></div>
		</td>
	</tr>
	<tr>
		<td>
			<div class="recaptcha_only_if_image">Enter the words above</div>
			<div class="recaptcha_only_if_audio">Enter the numbers you hear</div>
		</td>
		<td><input type="text" id="recaptcha_response_field" name="recaptcha_response_field" /></td>
		<td style="color: red;"><?php echo form_error('recaptcha_response_field'); ?></td>
		<?php echo $recaptcha_html; ?>
	</tr>
	<?php } else { ?>
	<tr>
		<td colspan="3">
			<p>Enter the code exactly as it appears:</p>
			<?php echo $captcha_html; ?>
		</td>
	</tr>
	<tr>
		<td><?php echo form_label('Confirmation Code', $captcha['id']); ?></td>
		<td><?php echo form_input($captcha); ?></td>
		<td style="color: red;"><?php echo form_error($captcha['name']); ?></td>
	</tr>
	<?php }
	} ?>
</table>
<?php echo form_submit('register', 'Register'); ?>
<br /><br />


<input type="checkbox" required="required" /> I agree with the <a href="<?php echo base_url().'page/39';?>">Terms of Use</a> of CILECT.<br />
<?php echo form_close(); ?>
</div><div class="fifty2"><h2>Who can register in CILECT</h2>
	To be eligible to register at the CILECT website, you must either be:
	<ul>
	<li>a CILECT (Full, Partner, Candidate) member school designated contact person; or</li>
		<li>a teacher in a CILECT member school.</li>
</ul>
	By registering,
	<ul>
	<li>school designated contact persons will be able to initially build and update at any time their school’s Profile; to upload material in the sections Knowledge, CAKE, News and Jobs; and to get access to all restricted sections;
	</li><li>teachers in CILECT member schools will be able to get access to all restricted sections.</li>
</ul>
	Registration of:
	<ul>
	<li>school designated contact persons is subject to authorization from the CILECT web administrators after verification with the respective school authorities;</li>
	<li>teachers is subject to authorization from the respective schools to which they belong.</li>
</ul>


</div>

<?php
$no['nologin']=1;
$this->load->view('template/rightbar',$no);  ?>
<?php $this->load->view('template/footer');  ?>
