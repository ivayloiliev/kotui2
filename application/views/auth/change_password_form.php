<?php
$old_password = array(
	'name'	=> 'old_password',
	'id'	=> 'old_password',
	'value' => set_value('old_password'),
	'size' 	=> 30,
	'placeholder' => 'Old password',
	'title' => 'Old password',
);
$new_password = array(
	'name'	=> 'new_password',
	'id'	=> 'new_password',
	'maxlength'	=> $this->config->item('password_max_length', 'tank_auth'),
	'size'	=> 30,
	'placeholder' => 'New password',
	'title' => 'New password',
);
$confirm_new_password = array(
	'name'	=> 'confirm_new_password',
	'id'	=> 'confirm_new_password',
	'maxlength'	=> $this->config->item('password_max_length', 'tank_auth'),
	'size' 	=> 30,
	'placeholder' => 'New password again',
	'title' => 'New password again',
);
?>
<?php $this->load->view('template/header');  ?>
    <div id="left">
<?php echo form_open($this->uri->uri_string()); ?>
<h1>Change Password</h1>
<table>
	<tr>

		<td><?php echo form_password($old_password); ?></td>
		<td style="color: red;"><?php echo form_error($old_password['name']); ?><?php echo isset($errors[$old_password['name']])?$errors[$old_password['name']]:''; ?></td>
	</tr>
	<tr>

		<td><?php echo form_password($new_password); ?></td>
		<td style="color: red;"><?php echo form_error($new_password['name']); ?><?php echo isset($errors[$new_password['name']])?$errors[$new_password['name']]:''; ?></td>
	</tr>
	<tr>

		<td><?php echo form_password($confirm_new_password); ?></td>
		<td style="color: red;"><?php echo form_error($confirm_new_password['name']); ?><?php echo isset($errors[$confirm_new_password['name']])?$errors[$confirm_new_password['name']]:''; ?></td>
	</tr>
</table>
<?php echo form_submit('change', 'Промяна на паролата'); ?>
<?php echo form_close(); ?>



<?php $this->load->view('template/rightbar');  ?>
<?php $this->load->view('template/footer');  ?>