<!DOCTYPE html>
<html lang="en" ng-app="kotui">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>Welcome to Kotui</title>
	<link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700&subset=latin,cyrillic-ext' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,400italic,500,500italic&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
	<script src='https://www.google.com/recaptcha/api.js'></script>
	<link href="<?=base_url();?>scripts/third-party/jquery-ui-1.11.4/jquery-ui.min.css" rel="stylesheet">
	<link href="<?=base_url();?>css/production.css" rel="stylesheet">
	<base href="/kotui2/">
	<script src="<?=base_url();?>scripts/public/main.js"></script>
</head>
<body>
	<header>
	    <h1>KOTUI</h1>
	    Word Puzzle Game
	</header>
<main ng-view>
</main>
<footer>
Word Puzzle Game. Developed by <a href="http://ivailoiliev.com">ivailoiliev.com</a>
</footer>
</body>
</html>
