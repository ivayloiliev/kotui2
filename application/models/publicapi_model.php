<?php
class publicapi_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
    function get_images_random($num){
        $this->db->order_by('rand()');
        $query = $this->db->get('images',$num);
        return $query->result();
    }
    function write_score($name,$score){
        $data = array(
      'name' => $name,
      'score' => $score);
      $this->db->insert('scores', $data);
    }
    function get_highscore(){
        $this->db->order_by('score','DESC');
        $query = $this->db->get('scores',5);
        return $query->result();
    }
} ?>
