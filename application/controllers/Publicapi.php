<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH.'libraries/REST_Controller.php');

class Publicapi extends REST_Controller {

	public function images_get(){
		$images=$this->publicapi_model->get_images_random(6);
		$this->response($images, 200);
	}
	public function highscores_post(){
		$name = $this->post('name');
    	$score = $this->post('score');
		if($name!="") $this->publicapi_model->write_score($name,$score);
		$high_scores=$this->publicapi_model->get_highscore();
		$this->response($high_scores, 200);
	}
}
