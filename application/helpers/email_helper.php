<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

function send_email($to, $subject, $message) {
	$CI =& get_instance();
	$CI->load->library('email');
	$CI->config->load('email');

	$from = $CI->config->item('smtp_user');
	$CI->email->from($from, $CI->user_model->getName());
	$CI->email->to($to); 
	$CI->email->subject($subject);
	$CI->email->message($message);	
	if(!$CI->email->send()) {
    		return false;
	}

	return true;
}


function get_message_body($invitee, $invitee_id, $pond_name, $pond_id, $invitation_id) {

	$data = array(
    		'invitee'=> $invitee,
            'invitee_id' => $invitee_id,
            'pond_name' => $pond_name,
            'pond_id' => $pond_id,
            'invitation_id' => $invitation_id
    	);

    $CI =& get_instance();
    $body = $CI->load->view('templates/invitation.php',$data,TRUE);
    return body;
}