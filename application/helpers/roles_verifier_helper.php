<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');


 function get_role_for_pond($user_id, $pond_id){

	$CI =& get_instance();
	$CI->db->where('pond_id',$pond_id);
	$CI->db->where('user_id',$user_id);
	$query = $CI->db->get('user_roles');
	if($query->num_rows()>0){
		$result = $query->result();
		$role = $result[0]->role_id;
		
		return $role;
	}else{
		return 0;
	}
}

function is_user_pond_administrator($user_id, $pond_id) {
	$res = get_role_for_pond($user_id, $pond_id);
	if($res == 3 || $res == 4) {
		return true;
	}
	return false;
	
}

function is_user_pond_owner($user_id, $pond_id) {
	$res = get_role_for_pond($user_id, $pond_id);
	if($res == 3) {
		return true;
	}
	return false;
}

function is_user_pond_employeer($user_id, $pond_id) {
	$res = get_role_for_pond($user_id, $pond_id);
	if($res == 3 || $res == 4 || $res == 2) {
		return true;
	}
	return false;
}

function is_valid_role($role_id) {
	//CHECK the role from loaded cache
	if(!is_numeric($role_id)) {
    	return false;
    }
	return true;
}
