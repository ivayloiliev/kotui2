<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if($this->tank_auth->is_logged_in()==true){
			$this->load->model('user_model');
		}else{
			redirect(base_url());
		}
	}
	public function registered()
	{
		if($this->tank_auth->is_logged_in()) {
			echo "1";
		}else{
			echo "0";
		}
	}
	public function getName(){
		$name=$this->user_model->getName();
		echo $name;
	}
}
