//Grunt is just JavaScript running in node, after all...
module.exports = function(grunt) {

  // All upfront config goes in a massive nested object.
  grunt.initConfig({
    // You can set arbitrary key-value pairs.
    distFolder: 'scripts/public',
    // You can also set the value of a key as parsed JSON.
    // Allows us to reference properties we declared in package.json.
    pkg: grunt.file.readJSON('package.json'),
    // Grunt tasks are associated with specific properties.
    // these names generally match their npm package name.
    concat: {
      // Specify some options, usually specific to each plugin.
      options: {
        // Specifies string to be inserted between concatenated files.
        separator: '\n\r;\n\r'
      },
      // 'dist' is what is called a "target."
      // It's a way of specifying different sub-tasks or modes.
      dist: {
        // The files to concatenate:
        // Notice the wildcard, which is automatically expanded.
        src: [  'scripts/third-party/jquery-2.1.4.min.js',
                'scripts/third-party/jquery-ui-1.11.4/jquery-ui.min.js',
                'scripts/third-party/angular.min.js',
                'scripts/third-party/angular-resource.min.js',
                'scripts/third-party/angular-route.min.js',
                'scripts/third-party/dropzone.js',
                'scripts/third-party/ng-map.min.js',
                'scripts/angular/app.js',
                'scripts/angular/services/*.js',
                'scripts/angular/config/*.js',
                'scripts/angular/controllers/*.js',
                'scripts/angular/directives/*.js',
                'scripts/angular/filters/*.js'
              ],
        // The destination file:
        // Notice the angle-bracketed ERB-like templating,
        // which allows you to reference other properties.
        // This is equivalent to 'dist/main.js'.
        dest: '<%= distFolder %>/main.js'
        // You can reference any grunt config property you want.
        // Ex: '<%= concat.options.separator %>' instead of ';'
      }
    },
    less: {
         development: {
             options: {
                 paths: ["assets/css"]
             },
             files: {"css/production.css": "css/dev.less"}
         }
     },
     watch: {
       files: ['scripts/angular/*.js','scripts/angular/controllers/*.js','scripts/angular/services/*.js','scripts/angular/filters/*.js','scripts/angular/directives/*.js','scripts/angular/views/*.html','scripts/angular/views/directives/*.html','css/dev.less'],
       tasks: ['concat','less']
     }
  }); // The end of grunt.initConfig
  // We've set up each task's configuration.
  // Now actually load the tasks.
  // This will do a lookup similar to node's require() function.
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-watch');
  //grunt.registerTask('default', ['less']);
  // Register our own custom task alias.
  grunt.registerTask('default', ['concat','less','watch']);
};
