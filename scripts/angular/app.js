'use strict';

var app = angular.module('kotui', ['ngResource', 'ngRoute']);
app.constant('constants', {
      homeDir: '/kotui2/'
});

app.config(['$routeProvider','$locationProvider', 'constants', '$httpProvider',
  function($routeProvider,$locationProvider, constants, $httpProvider) {

    var homeDir = constants.homeDir;

    // Configure route provider
    $routeProvider.
      when('/', {
        templateUrl: homeDir+'scripts/angular/views/landing.html',
        controller: 'landingController'
      }).
      when('/game', {
        templateUrl: homeDir+'scripts/angular/views/game.html',
        controller: 'gameController'
      }).
      otherwise({
        redirectTo: '/'
      });
      $locationProvider.html5Mode(true);
  }]);
