app.factory('nameService', function() {
 var name = '';
 function set(val) {
   name = val;
 }
 function get() {
  return name;
 }

 return {
  set: set,
  get: get
 };

});
