app.controller("gameController", ['nameService', '$scope', '$timeout', '$http', '$interval', '$location',
	function(nameService, $scope, $timeout, $http, $interval, $location) {
		$scope.score = 0;
		$scope.initialTime = 15;
		$scope.time = $scope.initialTime;
		$scope.images = [];
		$scope.loaded = false;
		$scope.currentLetter = 0;
		$scope.activeImageIndex = 0;
		$scope.wrong=false;
		$scope.gameOn = true;
		$scope.name = nameService.get();
		if($scope.name==""){
			$location.path( "/" );
		}
		$http.get('publicapi/images').success(function(data){
			$scope.images = data;
			$scope.loaded = true;
			$scope.prepareImage(0);
		});
		$scope.storeScore = function(){
			$scope.loaded = false;
			dataToSend = {
				'name':$scope.name,
				'score':$scope.score
			};
			$http.post('publicapi/highscores',dataToSend).success(function(data){
				$scope.loaded = true;
				$scope.highscores = data;
			});
		};
		$scope.prepareImage = function(index){
			$scope.activeImage = $scope.images[index];
			$scope.stopTimer();
			$scope.startTimer($scope.initialTime);
			$scope.letters = $scope.activeImage.title.split('');
			$scope.lettersVisible = [];
			$scope.currentLetter = 0;
			for(var i=0;i<$scope.letters.length;i++){
				$scope.lettersVisible[i]=false;
			}
		};
		$scope.endGame = function(){
			$scope.gameOn = false;
			$scope.stopTimer();
			$scope.storeScore();
		};
		$scope.startTimer = function(time){
			$scope.time = time;
			$scope.intervalPromise = $interval(function () {
				if($scope.time==1){
					$scope.time=0;
					$scope.endGame();
				}else{
					$scope.time=$scope.time-1;
				}
			}, 1000);
		};
		$scope.stopTimer = function(){
			$interval.cancel($scope.intervalPromise);
		};
		$scope.nextLetter = function(){

			$scope.wrong=false;
			//$scope.lettersVisible[$scope.currentLetter] = true;
			//TBD: Using JQuery to remove the lag
			$('.letter:nth-child('+($scope.currentLetter+1)+') div').addClass('visible');
			$scope.currentLetter++;
			if($scope.currentLetter==$scope.letters.length){
				if($scope.images.length-1==$scope.activeImageIndex){
					$scope.endGame();
				}else{
					//TBD:  Using JQuery to remove the lag
					$('.letter div').removeClass('visible');
					$scope.activeImageIndex++;
					$scope.score = $scope.score+10;
					$scope.score = $scope.score + $scope.time;
					$scope.prepareImage($scope.activeImageIndex);
				}
			}
		};
		$scope.wrongLetter = function(){
			if($scope.time>2)$scope.time--;
			$scope.wrong=true;
		};
		// KEYPRESS LISTENERS
		$(document).on('keydown', function(e){
		  if($scope.gameOn){
		      if (String.fromCharCode(e.which) == $scope.letters[$scope.currentLetter].toUpperCase())
		      {
				  // LETTER IS CORRECT
				  $scope.nextLetter();
			  }else{
				  // LETTER IS NOT CORRECT
				  $scope.wrongLetter();
			  }
		  }
	  });
}]);
