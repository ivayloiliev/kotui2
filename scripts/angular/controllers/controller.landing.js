app.controller("landingController", ['nameService', '$scope', '$timeout', '$location',
	function(nameService, $scope, $timeout, $location) {
		$scope.showMsg = false;
		$timeout(function(){
			$scope.showMsg = true;
		},300);
		$scope.beginGame = function(){
			nameService.set($scope.name);
			$location.path( "/game" );
		};
}]);
